defmodule Backend.Store do
    use Agent

    # This Agent is our Database
    # Here all the "queries" are performed

    # Data is stored in a tuple left side of tuple is all the seasons and leagues within that year
    # This is done to read all the data set once
    def start_link(data) do
        available = data
        |> Enum.map(fn x -> {x["Season"],x["Div"]} end)
        |> Enum.uniq 
        |> Enum.group_by(fn {season, _} -> season end, fn {_, league} -> league end)
        |> Enum.to_list

        # Start the agent
        Agent.start_link(fn -> {available, data} end, name: __MODULE__)
    end

    # Returns a tuple with the format {season, [array_leagues]} 
    # of all available leagues inside any season
    # The logic that perfoms this is written in start_link for reading all the data set once
    def get_league_season_available() do
        Agent.get(__MODULE__, & &1) |> elem(0)
    end


    # Returns a list with all the leagues and seasons that match the filter
    def get_by_season_and_league(season, league) do
        Agent.get(__MODULE__, & &1) 
        |> elem(1)
        |> Enum.filter(fn x -> 
            x["Div"] == league && x["Season"] == season
        end)
    end

end