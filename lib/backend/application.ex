defmodule Backend.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    
    #We load the csv file only once and send the data to Store Agent
    matches_data = Backend.CSVReader.open_csv()
    # List all child processes to be supervised
    children = [
      # Start the endpoint when the application starts
      BackendWeb.Endpoint,
      # Starts a worker by calling: Backend.Worker.start_link(arg)
      # {Backend.Worker, arg},
      {Backend.Store, matches_data}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Backend.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    BackendWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
