defmodule Backend.ProtoEncoder do

    def encode_to_iodata!(data) do
        data |> data.__struct__.encode()
    end

end