defmodule Match do
    use Protobuf, """
        message Match {
            string id = 1;
            string season = 2;
            string div = 3;
            string HomeTeam = 4;
            string AwayTeam = 5;
            string date = 6;
            string FTAG = 7;
            string FTHG = 8;
            string FRT = 9;
            string HTAG = 10;
            string HTHG = 11;
            string HTR = 12;
        }

        message Matches {
            repeated Match match = 1;
        }

    """
end