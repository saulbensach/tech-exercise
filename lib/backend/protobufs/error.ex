defmodule Error do
    use Protobuf, """
        message Error {
            string reason = 1;
        }

    """
end