defmodule Available do
    use Protobuf, """
        message SeasonLeague {
            string season = 1;
            repeated string leagues = 2;
        }

        message Seasons {
            repeated SeasonLeague seasons = 1;
        }

    """
end