defmodule Backend.CSVReader do
    
    # FROM https://www.football-data.co.uk/notes.txt
    # Div = League Division
    # Date = Match Date (dd/mm/yy)
    # Time = Time of match kick off
    # HomeTeam = Home Team
    # AwayTeam = Away Team
    # FTHG and HG = Full Time Home Team Goals
    # FTAG and AG = Full Time Away Team Goals
    # FTR and Res = Full Time Result (H=Home Win, D=Draw, A=Away Win)
    # HTHG = Half Time Home Team Goals
    # HTAG = Half Time Away Team Goals
    # HTR = Half Time Result (H=Home Win, D=Draw, A=Away Win)

    # Reads Data.csv in the directory "/priv" and
    def open_csv() do
        Path.join(:code.priv_dir(:backend), "Data.csv")
        |> Path.expand(__DIR__)
        |> File.stream!
        |> CSV.decode(headers: true)
        |> Enum.to_list
        |> Enum.map(fn {:ok, match} ->
            # I want to delete "" and replace it by id
            id = match[""]
            match
            |> Map.delete("")
            |> Map.put("Id", id)
        end)
    end

end