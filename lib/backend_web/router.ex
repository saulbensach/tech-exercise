defmodule BackendWeb.Router do
  use BackendWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api/", BackendWeb do
    pipe_through :api

    get "/", ApiController, :index
  end

  # Only two routes / for json output /proto for protocol buffers output
  scope "/api/json", BackendWeb do
    pipe_through :api
    
    get "/fetch", ApiController, :fetch
    get "/list", ApiController, :list
  end

  scope "/api/proto", BackendWeb do
    pipe_through :api
    
    get "/fetch", ApiController, :fetch_proto
    get "/list", ApiController, :list_proto
  end

end
