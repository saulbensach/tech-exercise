defmodule BackendWeb.ApiController do
    use BackendWeb, :controller

    alias Backend.Store

    # Index is the one that returns json
    # Checks for parameters and renders the data obtained in the Store

    # If no params are sended we just send leagues and seasons with data available
    # If any of the params are bad we send 200 with a message
    # If everything checks we send the seasons and leagues

    def index(conn, params) do
        conn
        |> put_status(200)
        |> json(
            %{
                "status": "OK",
            }
        )
    end

    def fetch(conn, params) do
        case params_valid?(params) do
            {false, err} -> 
                conn
                |> put_status(422)
                |> render("bad_params.json", error: err)
            {true, _} ->  
                matches_data = Store.get_by_season_and_league(params["season"], params["league"])
                render(conn, "matches.json", matches: matches_data)
        end
    end

    def list(conn, params) do
        if params == %{} do
            availables = Backend.Store.get_league_season_available
            render(conn, "available.json", data: availables)
        end
    end

    # Same as fetch but sends protobuf instead of json
    def fetch_proto(conn, params) do
        case params_valid?(params) do
            {false, err} -> 
                conn
                |> put_status(422)
                |> render("bad_params.proto", error: err)
            {true, _} ->  
                matches_data = Store.get_by_season_and_league(params["season"], params["league"])
                render(conn, "matches.proto", matches: matches_data)
        end
    end

    def list_proto(conn, params) do
        if params == %{} do
            availables = Backend.Store.get_league_season_available
            render(conn, "available.proto", data: availables)
        end
    end

    #Check if params sended by client are okay
    defp params_valid?(params) do
        if keys_valid?(params) do
            result = Backend.Store.get_league_season_available 
            |> Enum.map(&Tuple.to_list(&1)) 
            |> List.flatten
            |> params_value_valid?(params)
            {result, "league or season does not exists"} #if result is okay bad values is ignored
        else
            {false, "one or more keys are not valid"}
        end
    end

    #Check if any of the values in params are valid
    defp params_value_valid?(available_params_values, params) do
        params_values = params |> Map.values
        Enum.all?(params_values, fn key -> 
            Enum.member?(available_params_values, key) == true
        end)
    end

    # Checks if params contains all available keys
    defp keys_valid?(params) do
        # We only have 2 types of param keys
        available_keys = ["league", "season"]
        params_keys = params |> Map.keys
        if length(params_keys) < 2 do
            false
        else
            Enum.all?(available_keys, fn key -> 
                Enum.member?(params_keys, key) == true
            end)
        end
    end

end