defmodule BackendWeb.ApiView do
    use BackendWeb, :view

    # This file renders the data sended to json or protobuf

    def render("available.proto", %{data: season_league}) do
        Available.Seasons.new(seasons: render_many(season_league, BackendWeb.ApiView, "available_season.proto"))
    end

    def render("available_season.proto", %{api: {season, leagues}}) do
        Available.SeasonLeague.new(
            season: season,
            leagues: leagues
        )
    end

    def render("available.json", %{data: season_league}) do
        render_many(season_league, BackendWeb.ApiView, "available_season.json")
    end

    def render("available_season.json", %{api: {season, leagues}}) do
        %{
            season: season,
            leagues: leagues
        }
    end

    def render("bad_params.proto", %{error: err}) do
        Error.Error.new(reason: err)
    end

    def render("bad_params.json", %{error: err}) do
        %{
            error: err
        }
    end

    def render("matches.proto", %{matches: matches}) do
        Match.Matches.new(match: render_many(matches, BackendWeb.ApiView, "match.proto"))
    end

    def render("match.proto", %{api: match}) do
        Match.Match.new(
            id: match["Id"],
            season: match["Season"],
            div: match["Div"],
            HomeTeam: match["HomeTeam"],
            AwayTeam: match["AwayTeam"],
            date: match["Date"],
            FTAG: match["FTAG"],
            FTHG: match["FTHG"],
            FRT: match["FTR"],
            HTAG: match["HTAG"],
            HTHG: match["HTHG"],
            HTR: match["HTR"]
        )
    end

    def render("matches.json", %{matches: matches}) do
        render_many(matches, BackendWeb.ApiView, "match.json")
    end

    def render("match.json", %{api: match}) do
        %{
            id: match["Id"],
            date: match["Date"],
            season: match["Season"],
            div: match["Div"],
            HomeTeam: match["HomeTeam"],
            AwayTeam: match["AwayTeam"],
            FTAG: match["FTAG"],
            FTHG: match["FTHG"],
            FRT: match["FTR"],
            HTAG: match["HTAG"],
            HTHG: match["HTHG"],
            HTR: match["HTR"]
        }
    end

end