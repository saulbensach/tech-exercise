FROM bitwalker/alpine-elixir-phoenix:latest

COPY entry.sh .
COPY config/ ./config
COPY lib ./lib
COPY priv ./priv
COPY mix.exs .
COPY mix.lock .

EXPOSE 4000
ENV PORT=4000 
#MIX_ENV=prod

# Cache elixir deps
ADD mix.exs mix.lock ./
RUN mix do deps.get --only prod, deps.compile

RUN mix do compile

CMD ["./entry.sh"]
