# Tech Exercise!

Small HTTP API for gathering leagues and seasons data
The API is divided in two parts one for obtaining the results in plain JSON and the other for obtaining Protocol Buffers data.

This repo is divided in three branches, master, kubernetes and instrumentation
on master you will find the required tasks, on kubernetes and instrumentation the bonus.

This README changes on each branch for instructions.

## Requirements
  * Docker

To start the API:
  * execute start.sh with `sh start.sh`

When docker finishes you can make a GET call to `x.x.x.x/api` it should return `{"status": "OK"}` the x are the local ip address of your machine localhost does not work because docker stack exposes to 0.0.0.0 (more info about this: https://github.com/moby/moby/issues/32299)


## Consuming JSON API

List the league and season pairs for which there are results available
  + `GET /api/json/list`

Fetch the results for a specific league and season pair
  + `GET /api/json/fetch?season=:season&league=:league`

Both parameters :season and :league are required.
If parameters are wrong it will send `422` and an error message.

Example: `/api/json/fetch?season=201617&league=SP1`

## Consuming Protocol Buffers API

Exactly the same as JSON api but we replace `json` with `proto` in the url

List the league and season pairs for which there are results available
  + `GET /api/proto/list`

Fetch the results for a specific league and season pair
  + `GET /api/proto/fetch?season=:season&league=:league`

Both parameters :season and :league are required
If parameters are wrong it will send `422` and an error message.

Example: `/api/proto/fetch?season=201617&league=SP1`


## Development
The project is developed using Phoenix Framework, if you need to install follow this link https://hexdocs.pm/phoenix/installation.html#content

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix

## Other work

If you want to know all my Elixir work feel free to visit my repo `https://gitlab.com/arenastack-dev/backend` it was developed for my Startup